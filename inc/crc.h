/**
  ******************************************************************************
  * @file    crc.h
  * @author  adix79@gmail.com
  * @version v1.0.0
  * @date    10-11-2016
  * @brief   Header file of Lib_crc module.
  ******************************************************************************
  *    This file is part of Lib_crc.
  *
  *  Lib_crc is free software; you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation; either version 2 of the License, or
  *  (at your option) any later version.
  *
  *  Lib_crc is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with Lib_crc; if not, write to the Free Software
  ******************************************************************************
  */

#ifndef CRC_H_INC
#define CRC_H_INC

#include<stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
    False,True
} bool_t;

#define POLY32 0x04C11DB7
#define CRC32_SIZE 4

#define POLY16 0x8005
#define CRC16_SIZE 2

#define POLY8 0x7
#define CRC8_SIZE 1

#define MAX32 0xFFFFFFFF
#define MAX16 0xFFFF
#define MAX8  0xFF
#define ZERO 0x0

#define REF_IN True
#define REF_OUT True

#ifndef UINT32_MAX
typedef unsigned int uint32_t;
#endif // UINT32_MAX
#ifndef UINT16_MAX
typedef unsigned short uint16_t;
#endif // UINT16_MAX
#ifndef UINT8_MAX
typedef unsigned char uint8_t;
#endif // UINT8_MAX
/*
#undef LTABLE32
#undef LTABLE16
#undef LTABLE8
#undef LCACHE32
#undef LCACHE16
#undef LCACHE8
*/

/*
//TURN ON LTABLEX

#define LTABLE32
#define LTABLE16
#define LTABLE8
*/

/*
//TURN ON LCACHEX
#define LCACHE32
#define LCACHE16
#define LCACHE8
*/

/** \brief Compute crc
 *
 * \param bytes from which the crc is calculated
 * \param size of bytes
 * \param poly used polynomial
 * \param crc_size crc size 1 or 2 or 4 bytes
 * \param  init_value crc init value
 * \param  xor_value xor value
 * \param  reflect_in If True then It bounces byte during computing the crc
 * \param  reflect_out If True then It bounces crc before final xor
 * \return crc
 *
 */

uint32_t calculate_crc(const uint8_t* bytes,size_t size,uint32_t poly,size_t crc_size,uint32_t init_value,uint32_t xor_value,bool_t reflect_in,bool_t reflect_out);


 /** \brief LCACHEX if it's defined then can be use INIT_CRCX macro which cache all the crc range from (0..255)
  *\param p polynomial
  *
  */

#ifdef LCACHE32
void init_crc32(uint32_t poly);
#define INIT_CRC32(p) \
           init_crc32((p))
#else
#define INIT_CRC32(p)
#endif // LCACHE32


#ifdef LCACHE16
void init_crc16(uint16_t poly);
#define INIT_CRC16(p) \
           init_crc16((p))
#else
#define INIT_CRC16(p)
#endif // LCACHE16

#ifdef LCACHE8
void init_crc8(uint8_t poly);
#define INIT_CRC8(p) \
           init_crc8((p))
#else
#define INIT_CRC8(p)
#endif // LCACHE8


/** \brief Full CRC Macro
 *
 * \param @see crc(...) function
 * \return crc
 *
 */

#define __CRC__(b,s,p,cs,iv,xv,ri,ro) \
        calculate_crc((b),(s),(p),(cs),(iv),(xv),(ri),(ro))


/** \brief  Predefined CRC
 *
 * \param b bytes from which the crc is calculated
 * \param s size of bytes
 * \return crc
 *
 */

#define crc32(b,s) \
        __CRC__((b),(s),POLY32,CRC32_SIZE,MAX32,MAX32,REF_IN,REF_OUT)
#define crc16(b,s) \
        (uint16_t) __CRC__((b),(s),POLY16,CRC16_SIZE,ZERO,ZERO,REF_IN,REF_OUT)
#define crc16_modbus(b,s) \
        (uint16_t) __CRC__((b),(s),POLY16,CRC16_SIZE,MAX16,ZERO,REF_IN,REF_OUT)
#define crc16_usb(b,s) \
        (uint16_t) __CRC__((b),(s),POLY16,CRC16_SIZE,MAX16,MAX16,REF_IN,REF_OUT)
#define crc8(b,s) \
        (uint8_t)__CRC__((b),(s),POLY8,CRC8_SIZE,ZERO,ZERO,!REF_IN,!REF_OUT)

#ifdef __cplusplus
}
#endif
#endif // CRC_H_INC
